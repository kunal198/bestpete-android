package com.gabar.bestpete.database;

import android.accounts.Account;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by brst-pc81 on 12/29/17.
 */
@Dao
public interface CategoryDAO {

    @Query("SELECT * FROM category")
    List<Category> getAllCategories();

    @Query("SELECT * FROM category ORDER BY id DESC LIMIT 1")
    Category getLastCategory();

    @Insert
    void insert(Category category);

    @Delete
    void delete(Category category);

    @Insert
    void insertAccounts(AddAccounts accounts);


}
