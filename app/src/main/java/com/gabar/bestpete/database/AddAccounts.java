package com.gabar.bestpete.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

/**
 * Created by brst-pc81 on 1/12/18.
 */
@Entity(tableName = "account_names")
public class AddAccounts {

    @PrimaryKey(autoGenerate = true)
    @SerializedName("accountId")
    private long accountId;

    @ColumnInfo(name = "catId")
    @SerializedName("catId")
    private int catId;

    @ColumnInfo(name = "account_name")
    @SerializedName("account_name")
    String account_name;

    @ColumnInfo(name = "image")
    @SerializedName("image")
    int account_image;

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public int getAccount_image() {
        return account_image;
    }

    public void setAccount_image(int account_image) {
        this.account_image = account_image;
    }

}
