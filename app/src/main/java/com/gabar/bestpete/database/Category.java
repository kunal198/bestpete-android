package com.gabar.bestpete.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by brst-pc81 on 12/29/17.
 */
@Entity(tableName = "category")
public class Category {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    @SerializedName("id")
    private int id;


    @ColumnInfo(name = "cat_name")
    @SerializedName("cat_name")
    private String cat_name;


    @ColumnInfo(name = "accounts")
    @SerializedName("accounts")
    @Ignore
    private List<AddAccounts> accounts;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    public List<AddAccounts> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<AddAccounts> accounts) {
        this.accounts = accounts;
    }
}
