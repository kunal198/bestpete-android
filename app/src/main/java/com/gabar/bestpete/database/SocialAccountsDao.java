package com.gabar.bestpete.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by brst-pc81 on 12/29/17.
 */

@Dao
public interface SocialAccountsDao {

    @Insert
    void insert(AddAccounts accounts);

    @Query("SELECT * FROM account_names where catId = :catId")
    List<AddAccounts> getAllAccounts(int catId);


    @Delete
    void delete(AddAccounts account);



}
