package com.gabar.bestpete.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by brst-pc81 on 12/29/17.
 */
@Entity(tableName = "socialAccount")
public class SocialAccounts {

    @PrimaryKey(autoGenerate = true)
    private String accountId;

    @ColumnInfo(name = "catId")
    private int catId;

    @ColumnInfo(name = "facebookAccount")
    private String facebookAccount;
    @ColumnInfo(name = "twitterAccount")
    private String twitterAccount;
    @ColumnInfo(name = "instaAccount")
    private String instaAccount;



    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getFacebookAccount() {
        return facebookAccount;
    }

    public void setFacebookAccount(String facebookAccount) {
        this.facebookAccount = facebookAccount;
    }

    public String getTwitterAccount() {
        return twitterAccount;
    }

    public void setTwitterAccount(String twitterAccount) {
        this.twitterAccount = twitterAccount;
    }

    public String getInstaAccount() {
        return instaAccount;
    }

    public void setInstaAccount(String instaAccount) {
        this.instaAccount = instaAccount;
    }

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }
}
