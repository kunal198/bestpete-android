package com.gabar.bestpete.activities;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.gabar.bestpete.R;
import com.gabar.bestpete.adapters.AddAcountsAdapter;
import com.gabar.bestpete.database.AddAccounts;
import com.gabar.bestpete.database.AppDatabase;
import com.gabar.bestpete.fragments.MyTwitterApiClient;
import com.gabar.bestpete.models.DataModel;
import com.gabar.bestpete.twitter.WebService;
import com.gabar.bestpete.utilities.Constants;
import com.gabar.bestpete.utilities.GlobalUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterSession;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;

public class AddSocialAccountsActivity extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    ImageView imgAddAccounts;
    SwipeMenuListView listView;
    AddAcountsAdapter mAdapter;
    List<AddAccounts> list = new ArrayList<>();
    ArrayList<DataModel> pagesList = new ArrayList<>();
    ArrayList<String> twitterAccountsList = new ArrayList<>();
    String accountName;
    int cat_id;
    AppDatabase database;
    AutoCompleteTextView auto_pages;
    ArrayAdapter<String> auto_twitter_adapter;
    ArrayAdapter<DataModel> auto_facebook_adapter;
    String accountResult;
    boolean selected = false;
    String value = " ";
    AppCompatSpinner socialAccountsSpinner;
    long facebook_page_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_social_accounts);

        Intent in = getIntent();
        cat_id = in.getIntExtra(Constants.CAT_ID, 0);

        database = AppDatabase.getAppDatabase(AddSocialAccountsActivity.this);
        list = database.socialDao().getAllAccounts(cat_id);

        initViews();
        addActionListner();

        setmAdapter();


    }

    private void initViews() {
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Social Accounts");
        setSupportActionBar(toolbar);
        GlobalUtils.getInstance().centerToolbarTitle(toolbar);
        imgAddAccounts = findViewById(R.id.imgAddAccounts);
        listView = findViewById(R.id.listView);

    }

    private void addActionListner() {
        imgAddAccounts.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgAddAccounts:
                addAccountDialog();
                break;
        }
    }

    private void setmAdapter() {

        mAdapter = new AddAcountsAdapter(AddSocialAccountsActivity.this, list);
        listView.setAdapter(mAdapter);
        listView.setMenuCreator(swipeCreator());
        listView.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);

        listView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {

                if (list.size() > 0) {
                    AddAccounts acc = list.get(position);
                    database.socialDao().delete(acc);
                    mAdapter.removeItem(position, listView);
                    mAdapter = new AddAcountsAdapter(AddSocialAccountsActivity.this, list);
                    listView.setAdapter(mAdapter);
                    listView.setMenuCreator(swipeCreator());
                    listView.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);
                }

                return false;
            }
        });
    }
    public SwipeMenuCreator swipeCreator() {
        SwipeMenuCreator swipeMenuCreator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {

                SwipeMenuItem openItem = new SwipeMenuItem(AddSocialAccountsActivity.this);
                openItem.setBackground(new ColorDrawable(Color.rgb(254, 2, 2)));
                openItem.setWidth(200);
                openItem.setTitle("Delete");
                openItem.setTitleSize(15);
                openItem.setTitleColor(Color.WHITE);
                menu.addMenuItem(openItem);
            }
        };
        return swipeMenuCreator;
    }


    private void addAccountDialog() {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.search_pages_dialog);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.setCancelable(true);
        socialAccountsSpinner = dialog.findViewById(R.id.socialAccountsSpinner);
        String[] arraySpinner = new String[]{

                "Facebook", "Twitter", "Instagram"

        };
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arraySpinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        socialAccountsSpinner.setAdapter(adapter);

        auto_pages = dialog.findViewById(R.id.auto_pages);
        auto_pages.addTextChangedListener(textWatcher);

        auto_pages.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                String item=adapterView.getItemAtPosition(i).toString();
                 facebook_page_id= pagesList.get(i).getId();

                Log.d("pos====",facebook_page_id+"");
            }
        });
        Button btnAdd_account = dialog.findViewById(R.id.btnAdd_account);
        btnAdd_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                accountName = auto_pages.getText().toString();

                if (!accountName.isEmpty()) {

                    if (socialAccountsSpinner.getSelectedItem().equals("Facebook")) {
                        addAccounts(cat_id, database, facebook_page_id, accountName, R.drawable.ic_facebook);

                        Log.d("accounts--",accountName+"=="+facebook_page_id);

                        AddAccounts accounts = new AddAccounts();
                        accounts.setAccount_name(accountName);
                        accounts.setAccount_image(R.drawable.ic_facebook);

                        list.add(accounts);
                        mAdapter.notifyCategories(listView);
                        dialog.dismiss();
                    } else if (socialAccountsSpinner.getSelectedItem().equals("Twitter")) {
                       // addAccounts(cat_id, database,facebook_page_id, accountName, R.drawable.ic_twitter);
                        AddAccounts accounts = new AddAccounts();
                        accounts.setAccount_name(accountName);
                        accounts.setAccount_image(R.drawable.ic_twitter);
                        list.add(accounts);
                        mAdapter.notifyCategories(listView);
                        dialog.dismiss();
                    }
                }
            }
        });
        dialog.getWindow().setAttributes(lp);
        dialog.show();
    }
    private AddAccounts insertAccounts(AppDatabase database, AddAccounts accounts) {
        database.socialDao().insert(accounts);
        return accounts;
    }
    private void addAccounts(int id, AppDatabase database,long faceebook_account_id, String accountName, int account_image) {
        AddAccounts acc = new AddAccounts();
        acc.setCatId(id);
        acc.setAccountId(faceebook_account_id);
        acc.setAccount_name(accountName);
        acc.setAccount_image(account_image);
        insertAccounts(database, acc);

    }
    private void facebookPages(String query) {

        GraphRequest graphRequest = GraphRequest.newGraphPathRequest(AccessToken.getCurrentAccessToken(), "search?q=" + query + "&type=page", new GraphRequest.Callback() {
            @Override
            public void onCompleted(final GraphResponse response) {
                if (response != null) {

                        Log.d("PAGES", "" + response.getJSONObject());
                        pagesList.clear();
                        JSONObject jsonObject = response.getJSONObject();
                        if (jsonObject != null) {

                            JSONArray jsonArray = jsonObject.optJSONArray("data");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                DataModel model=new DataModel();

                                JSONObject innerObj = jsonArray.optJSONObject(i);
                                String page_name = innerObj.optString("name");
                                facebook_page_id = Long.parseLong(innerObj.optString("id"));
                                model.setId(facebook_page_id);
                                model.setName(page_name);
                                pagesList.add(model);

                            }

                            auto_facebook_adapter = new ArrayAdapter<>(AddSocialAccountsActivity.this,
                                    android.R.layout.simple_dropdown_item_1line, pagesList);
                            auto_pages.setAdapter(auto_facebook_adapter);
                            auto_pages.setThreshold(3);
                            auto_pages.showDropDown();
                        }

                }
            }
        });
        graphRequest.executeAsync();

    }
    public void getTwitterAccounts(String account) {
        final TwitterSession activeSession = TwitterCore.getInstance().getSessionManager().getActiveSession();
        MyTwitterApiClient myTwitterApiClient = new MyTwitterApiClient(activeSession);
        final WebService service = myTwitterApiClient.getCustomService();
        Call<JsonArray> call = service.getTwitterAccounts(account);
        call.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, retrofit2.Response<JsonArray> response) {
                if (response.body() != null) {
                    twitterAccountsList.clear();
                    Log.d("ACCOUNTS : ", "==" + response.body());
                    Gson gson = new GsonBuilder().create();
                    JsonArray customArray = (JsonArray) gson.toJsonTree(response.body());
                    JSONArray jsonArray = null;
                    try {
                        jsonArray = new JSONArray(customArray.toString());

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.optJSONObject(i);
                            Log.d("Account_name", "==" + jsonObject.optString("screen_name"));
                            accountResult = jsonObject.optString("screen_name");
                            twitterAccountsList.add(accountResult);

                        }
                        auto_twitter_adapter = new ArrayAdapter<>(AddSocialAccountsActivity.this,
                                android.R.layout.simple_dropdown_item_1line, twitterAccountsList);
                        auto_pages.setAdapter(auto_twitter_adapter);
                        auto_pages.setThreshold(3);
                        auto_pages.showDropDown();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                Log.d("ACCOUNTS : ", "onFailure" + t);
            }
        });
    }
    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
            if (value.equalsIgnoreCase(s.toString())) {
                selected = true;
            } else {
                selected = false;
            }

        }
        @Override
        public void afterTextChanged(Editable editable) {
            // TODO Auto-generated method stub
            if (socialAccountsSpinner.getSelectedItem().equals("Facebook")) {


                if (!selected) {
                    if (editable.length() >= 3) {
                        value = editable.toString();
                        facebookPages(editable.toString());
                    }
                }
            }
            if (socialAccountsSpinner.getSelectedItem().equals("Twitter")) {
                if (!selected) {
                    if (editable.length() >= 3) {

                        value = editable.toString();
                        getTwitterAccounts(editable.toString());

                    }
                }
            }
            if (socialAccountsSpinner.getSelectedItem().equals("Instagram")) {
                Toast.makeText(AddSocialAccountsActivity.this, "In Development", Toast.LENGTH_SHORT).show();
            }
        }
    };

}
