package com.gabar.bestpete.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.gabar.bestpete.R;
import com.gabar.bestpete.utilities.Constants;
import com.gabar.bestpete.utilities.GlobalUtils;
import com.gabar.bestpete.utilities.SharedPrefUtil;

public class SplashActivity extends AppCompatActivity {
    public static int DURATION=2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (GlobalUtils.getInstance().isNetworkConnected(SplashActivity.this)) {

                    String instaID=new SharedPrefUtil(SplashActivity.this).getString(Constants.INSTA_ID);
                    String twitterID=new SharedPrefUtil(SplashActivity.this).getString(Constants.TWITTER_ID);
                    String fbID=new SharedPrefUtil(SplashActivity.this).getString(Constants.fB_ID);
                    if (instaID!=null||twitterID!=null||fbID!=null){
                        startActivity(new Intent(SplashActivity.this, MainActivity.class));
                        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                        finish();
                    }else {
                        startActivity(new Intent(SplashActivity.this, WelcomeActivity.class));
                        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                        finish();
                    }
                }else{

                    GlobalUtils.getInstance().ShowNoIternetDialog(SplashActivity.this,"NO Internet Connected...");

                }

            }
        },DURATION);
    }
}
