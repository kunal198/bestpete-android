package com.gabar.bestpete.activities;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.LoggingBehavior;
import com.gabar.bestpete.R;
import com.gabar.bestpete.instgramlogin.InstagramApp;
import com.gabar.bestpete.utilities.Constants;
import com.gabar.bestpete.utilities.GlobalUtils;
import com.gabar.bestpete.utilities.SharedPrefUtil;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener{
    Toolbar toolbar;
    ImageView img_fb,img_twitter,img_insta;

    public CallbackManager callbackManager;
    private InstagramApp mApp;
    TwitterAuthClient authClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(this);
        super.onCreate(savedInstanceState);
        FacebookSdk.setIsDebugEnabled(true);
        callbackManager = CallbackManager.Factory.create();
        FacebookSdk.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
        setContentView(R.layout.activity_login);

        TwitterConfig config = new TwitterConfig.Builder(this)
                .logger(new DefaultLogger(Log.DEBUG))
                .twitterAuthConfig(new TwitterAuthConfig(Constants.TWITTER_KEY, Constants.TWITTER_SECRET))
                .debug(true)
                .build();
        Twitter.initialize(config);

        mApp = new InstagramApp(this, Constants.CLIENT_ID,
                Constants.CLIENT_SECRET, Constants.CALLBACK_URL);
        mApp.setListener(new InstagramApp.OAuthAuthenticationListener() {
            @Override
            public void onSuccess() {
                Toast.makeText(LoginActivity.this, "Username: "+mApp.getUserName(), Toast.LENGTH_SHORT).show();
                String instaId=mApp.getId();
                new SharedPrefUtil(LoginActivity.this).saveString(Constants.INSTA_ID,instaId);
                new SharedPrefUtil(LoginActivity.this).saveBoolean(Constants.INSTA_ACTIVE,true);
                startActivity(new Intent(LoginActivity.this,MainActivity.class));
                overridePendingTransition(R.anim.slide_in,R.anim.slide_out);
                finish();
            }

            @Override
            public void onFail(String error) {
                Toast.makeText(LoginActivity.this, error, Toast.LENGTH_SHORT)
                        .show();
            }
        });

        init();
        listners();


    }

    public void connectOrDisconnectUser() {
        if (!mApp.hasAccessToken()) {
            mApp.authorize();
           // mApp.getUsers();
        }
    }
    private void init(){
        toolbar=findViewById(R.id.toolbar);
        toolbar.setTitle("Login");
        setSupportActionBar(toolbar);
        GlobalUtils.getInstance().centerToolbarTitle(toolbar);
        img_fb=findViewById(R.id.img_fb);
        img_twitter=findViewById(R.id.img_twitter);
        img_insta=findViewById(R.id.img_insta);
    }
    private void listners(){
        img_fb.setOnClickListener(this);
        img_twitter.setOnClickListener(this);
        img_insta.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.img_fb:
                GlobalUtils.getInstance().facebookLogin(this,callbackManager);
                break;
            case R.id.img_twitter:
                authClient = new TwitterAuthClient();
               GlobalUtils.getInstance().twitterAuth(LoginActivity.this,authClient);
                break;

            case R.id.img_insta:
                connectOrDisconnectUser();
                break;

        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TwitterAuthConfig.DEFAULT_AUTH_REQUEST_CODE) {
            authClient.onActivityResult(requestCode, resultCode, data);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }


}
