package com.gabar.bestpete.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.gabar.bestpete.R;
import com.gabar.bestpete.fragments.AccountFragment;
import com.gabar.bestpete.fragments.CategoriesFragment;
import com.gabar.bestpete.fragments.FeedsFragment;
import com.gabar.bestpete.fragments.HelpFragment;
import com.gabar.bestpete.utilities.Constants;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

public class MainActivity extends AppCompatActivity {
    public CallbackManager callbackManager;
    private static final int TIME_INTERVAL = 2000;
    private long mBackPressed;
    public BottomNavigationView navigationbar;
    FragmentManager fragmentManager;
    private Fragment fragment;
    TwitterAuthClient authClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        callbackManager = CallbackManager.Factory.create();


        TwitterConfig config = new TwitterConfig.Builder(this)
                .logger(new DefaultLogger(Log.DEBUG))
                .twitterAuthConfig(new TwitterAuthConfig(Constants.TWITTER_KEY, Constants.TWITTER_SECRET))
                .debug(true)
                .build();
        Twitter.initialize(config);

        authClient=new TwitterAuthClient();

        navigationbar = (BottomNavigationView) findViewById(R.id.navigationbar);
        navigationbar.getMenu().getItem(0).setChecked(true);

        fragmentManager = getSupportFragmentManager();
        fragment = new CategoriesFragment();
        final FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.main_container, fragment).commit();

        navigationbar.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.categories:
                        fragment = new CategoriesFragment();
                        break;
                    case R.id.feeds:
                        fragment = new FeedsFragment();

                        break;
                    case R.id.account:
                        fragment = AccountFragment.getInstance(callbackManager,authClient);
                        break;
                    case R.id.help:
                        fragment = new HelpFragment();
                        break;
                }

                final FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.main_container, fragment).commit();
                return true;

            }
        });
    }

    @Override
    public void onBackPressed() {
        if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
            super.onBackPressed();
            return;
        } else {
            Toast.makeText(this, "Tap again to exit.", Toast.LENGTH_SHORT).show();
        }
        mBackPressed = System.currentTimeMillis();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TwitterAuthConfig.DEFAULT_AUTH_REQUEST_CODE) {

            authClient.onActivityResult(requestCode, resultCode, data);

        } else {

            callbackManager.onActivityResult(requestCode, resultCode, data);

        }
    }

}