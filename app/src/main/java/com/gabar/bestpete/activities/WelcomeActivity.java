package com.gabar.bestpete.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;

import com.gabar.bestpete.R;
import com.gabar.bestpete.utilities.GlobalUtils;

public class WelcomeActivity extends AppCompatActivity implements View.OnClickListener{
    Toolbar toolbar;
    LinearLayout lin_continue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        toolbar=findViewById(R.id.toolbar);
        toolbar.setTitle("Welcome to Bestpete");
        setSupportActionBar(toolbar);
        GlobalUtils.getInstance().centerToolbarTitle(toolbar);
        init();
        listner();
    }

    private void init(){
        lin_continue=findViewById(R.id.lin_continue);
    }

    private void listner(){
        lin_continue.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.lin_continue:
                startActivity(new Intent(WelcomeActivity.this,LoginActivity.class));
                overridePendingTransition(R.anim.slide_in,R.anim.slide_out);
                finish();
        }
    }
}
