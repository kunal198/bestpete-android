package com.gabar.bestpete.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.Image;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.gabar.bestpete.R;
import com.gabar.bestpete.activities.AddSocialAccountsActivity;
import com.gabar.bestpete.activities.MainActivity;
import com.gabar.bestpete.database.AddAccounts;
import com.gabar.bestpete.database.AppDatabase;
import com.gabar.bestpete.database.Category;
import com.gabar.bestpete.database.SocialAccounts;
import com.gabar.bestpete.fragments.FeedsFragment;
import com.gabar.bestpete.utilities.BoldTextView;
import com.gabar.bestpete.utilities.RegularTextView;

import java.util.List;

/**
 * Created by brst-pc81 on 12/21/17.
 */

public class AddAcountsAdapter extends BaseAdapter {

    List<AddAccounts> list;
    LayoutInflater inflater;
    Context context;

    public AddAcountsAdapter(Context context, List<AddAccounts> list) {
        this.list = list;
        this.context = context;
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {
        MyViewHolder mViewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.inflater_add_accounts, viewGroup, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }
        mViewHolder.tvTitle.setText(list.get(i).getAccount_name());
        mViewHolder.account_image.setImageResource(list.get(i).getAccount_image());

        return convertView;
    }

    private class MyViewHolder {
        TextView tvTitle;
        ImageView account_image;

        public MyViewHolder(View item) {
            tvTitle=item.findViewById(R.id.account_name);
            account_image=item.findViewById(R.id.account_image);

        }
    }
    public void notifyCategories(SwipeMenuListView listview) {

        notifyDataSetChanged();
    }
    public void removeItem(int position, SwipeMenuListView listview) {
        list.remove(position);
        notifyDataSetChanged();

    }


}
