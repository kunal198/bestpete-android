package com.gabar.bestpete.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;

import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.gabar.bestpete.R;
import com.gabar.bestpete.activities.AddSocialAccountsActivity;
import com.gabar.bestpete.activities.MainActivity;
import com.gabar.bestpete.database.Category;
import com.gabar.bestpete.fragments.FeedsFragment;
import com.gabar.bestpete.utilities.BoldTextView;
import com.gabar.bestpete.utilities.Constants;
import com.gabar.bestpete.utilities.RegularTextView;
import java.util.List;

/**
 * Created by brst-pc81 on 12/21/17.
 */

public class CategoriesListAdapter extends BaseAdapter {

    List<Category> list;
    LayoutInflater inflater;
    Context context;
    FragmentManager fragmentManager;
    Fragment fragment;
    int id;


    public CategoriesListAdapter(Context context,   List<Category> list) {
        this.list = list;
        this.context = context;
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {
        MyViewHolder mViewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.inflater_category_item_list, viewGroup, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }
        mViewHolder.tvTitle.setText(list.get(i).getCat_name());

        mViewHolder.cat_arrow_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                id = list.get(i).getId();
                Bundle bundle=new Bundle();
                fragmentManager = ((MainActivity) context).getSupportFragmentManager();
                fragment = new FeedsFragment();
                bundle.putInt("Category_id",id);
                fragment.setArguments(bundle);
                final FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.main_container, fragment).commit();
                ((MainActivity) context).navigationbar.getMenu().getItem(1).setChecked(true);
            }
        });



        mViewHolder.tvTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                id = list.get(i).getId();
                //addAccountDialog(id);
                context.startActivity(new Intent(context, AddSocialAccountsActivity.class).putExtra(Constants.CAT_ID,id));
            }
        });

        return convertView;
    }

    private class MyViewHolder {
        RegularTextView tvTitle;
        ImageView cat_arrow_image;

        public MyViewHolder(View item) {
            tvTitle = (RegularTextView) item.findViewById(R.id.textViewCateoryName);
            cat_arrow_image = (ImageView) item.findViewById(R.id.cat_arrow_image);


        }
    }
/*    private void addAccountDialog(int id) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.show_accounts);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setAttributes(lp);
        final EditText mEdFb= (EditText)dialog.findViewById(R.id.edt_facebook_account);
        final EditText mEdInsta= (EditText)dialog.findViewById(R.id.edt_insta_account);
        final EditText mEdTw= (EditText)dialog.findViewById(R.id.edt_twitter_account);

        disableEditText(mEdFb);
        disableEditText(mEdInsta);
        disableEditText(mEdTw);

         if(mDbHelper == null){
             mDbHelper = AppDatabase.getAppDatabase(context);
         }
        SocialAccounts mObject =  mDbHelper.socialDao().getAllAccounts(id);

        mEdFb.setText(mObject.getFacebookAccount());
        mEdInsta.setText(mObject.getInstaAccount());
        mEdTw.setText(mObject.getTwitterAccount());

        Button add_button=dialog.findViewById(R.id.add_button);
        add_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });



        dialog.show();

    }*/
    public void notifyCategories(SwipeMenuListView listview, BoldTextView noCategory) {

        notifyDataSetChanged();
        noCategory.setVisibility(View.GONE);
        listview.setVisibility(View.VISIBLE);

    }
    public void removeItem(int position, SwipeMenuListView listview, BoldTextView noCategory) {
        list.remove(position);
        notifyDataSetChanged();

        if (list.size() == 0) {
            noCategory.setVisibility(View.VISIBLE);
            listview.setVisibility(View.GONE);
        }
    }


    private void disableEditText(EditText editText) {
        editText.setFocusable(false);
        editText.setEnabled(false);
        editText.setCursorVisible(false);
        editText.setKeyListener(null);
        editText.setBackgroundColor(Color.TRANSPARENT);
    }


}
