package com.gabar.bestpete.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gabar.bestpete.R;
import com.gabar.bestpete.utilities.RegularTextView;

import java.util.List;

/**
 * Created by brst-pc81 on 12/21/17.
 */

public class FeedsAdapter extends RecyclerView.Adapter<FeedsAdapter.MyViewHolder> {

    Context context;
    List<String> FeedsList;

    public FeedsAdapter(Context context, List<String> feedsList) {
        this.context = context;
        FeedsList = feedsList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

               View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.inflater_feed_items, parent, false);

               MyViewHolder vh = new MyViewHolder(itemView, viewType);


        return vh;

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.regularTextViewmessage.setText(FeedsList.get(position));

    }

    @Override
    public int getItemCount() {
        return FeedsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        RegularTextView regularTextViewmessage;
        public MyViewHolder(View itemView,int viewType) {
            super(itemView);

            regularTextViewmessage=itemView.findViewById(R.id.regularTextViewmessage);
        }
    }


}
