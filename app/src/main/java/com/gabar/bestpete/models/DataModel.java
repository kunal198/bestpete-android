package com.gabar.bestpete.models;

/**
 * Created by brst-pc81 on 1/22/18.
 */

public class DataModel {

    long id;
    String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString(){
        return name;
    }
}
