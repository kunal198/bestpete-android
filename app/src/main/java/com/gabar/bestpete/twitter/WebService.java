package com.gabar.bestpete.twitter;

import com.google.gson.JsonArray;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface WebService {

    @GET("https://api.twitter.com/1.1/statuses/user_timeline.json")
    Call<JsonArray> getUserFeeds(@Query("user_id") String user_id);


    @GET("https://api.twitter.com/1.1/users/search.json")
    Call<JsonArray> getTwitterAccounts(@Query("q") String q);
}
