package com.gabar.bestpete.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gabar.bestpete.R;
import com.gabar.bestpete.activities.MainActivity;
import com.gabar.bestpete.utilities.GlobalUtils;


public class HelpFragment extends Fragment {
    Toolbar toolbar;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View itemView=inflater.inflate(R.layout.fragment_help, container, false);
        toolbar = itemView.findViewById(R.id.toolbar);
        toolbar.setTitle("Help");
        ((MainActivity) getActivity()).setSupportActionBar(toolbar);
        GlobalUtils.getInstance().centerToolbarTitle(toolbar);

        return itemView;
    }
}
