package com.gabar.bestpete.fragments;

import com.gabar.bestpete.twitter.WebService;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterSession;

/**
 * Created by brst-pc81 on 1/1/18.
 */

public class MyTwitterApiClient extends TwitterApiClient {

    public MyTwitterApiClient(TwitterSession session) {
        super(session);
    }

    public WebService getCustomService() {
        return getService(WebService.class);
    }
}
