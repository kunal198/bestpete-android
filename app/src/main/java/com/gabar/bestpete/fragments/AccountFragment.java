package com.gabar.bestpete.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.login.LoginManager;
import com.gabar.bestpete.R;
import com.gabar.bestpete.activities.LoginActivity;
import com.gabar.bestpete.activities.MainActivity;
import com.gabar.bestpete.instgramlogin.InstagramApp;
import com.gabar.bestpete.utilities.Constants;
import com.gabar.bestpete.utilities.GlobalUtils;
import com.gabar.bestpete.utilities.SharedPrefUtil;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

public class AccountFragment extends Fragment implements View.OnClickListener{
    public static CallbackManager mCallbackManager;
    public static TwitterAuthClient mAuthClient;
    TwitterAuthClient authClient;



    Toolbar toolbar;
    ImageView img_fb_link, img_twitter_link, img_insta_link, image_fb_tick, img_twitter_tick, image_insta_tick;

    boolean fb_active;
    boolean twitter_active;
    boolean insta_active;

    InstagramApp mApp;

     public  static AccountFragment getInstance(CallbackManager callbackManager,TwitterAuthClient authClient){
         mCallbackManager=callbackManager;
         mAuthClient=authClient;
         return new AccountFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View itemView = inflater.inflate(R.layout.fragment_account, container, false);

        TwitterConfig config = new TwitterConfig.Builder(getContext())
                .logger(new DefaultLogger(Log.DEBUG))
                .twitterAuthConfig(new TwitterAuthConfig(Constants.TWITTER_KEY, Constants.TWITTER_SECRET))
                .debug(true)
                .build();
        Twitter.initialize(config);

        init(itemView);
        listners();

        onStart();



        String fbUSrID=new SharedPrefUtil(getContext()).getString(Constants.fB_ID);
        String twitterUSerId=new SharedPrefUtil(getContext()).getString(Constants.TWITTER_ID);
        String instaUSerID=new SharedPrefUtil(getContext()).getString(Constants.INSTA_ID);

        if (fbUSrID!=null){
            image_fb_tick.setVisibility(View.VISIBLE);
        }
        if (twitterUSerId!=null){
            img_twitter_tick.setVisibility(View.VISIBLE);
        }

        if (instaUSerID!=null) {

            image_insta_tick.setVisibility(View.VISIBLE);
        }

        return itemView;
    }

    public void connectOrDisconnectUser() {
        if (!mApp.hasAccessToken()) {
            mApp.authorize();

        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mApp = new InstagramApp(getContext(), Constants.CLIENT_ID,
                Constants.CLIENT_SECRET, Constants.CALLBACK_URL);


        mApp.setListener(new InstagramApp.OAuthAuthenticationListener() {
            @Override
            public void onSuccess() {
                String instaId=mApp.getId();
                new SharedPrefUtil(getContext()).saveString(Constants.INSTA_ID,instaId);
                new SharedPrefUtil(getContext()).saveBoolean(Constants.INSTA_ACTIVE,true);
                image_insta_tick.setVisibility(View.VISIBLE);
                img_insta_link.setImageResource(R.drawable.insta_link_f);

            }

            @Override
            public void onFail(String error) {
                Toast.makeText(getContext(), error, Toast.LENGTH_SHORT)
                        .show();
            }
        });
    }

    private void init(View view) {
        toolbar = view.findViewById(R.id.toolbar);
        toolbar.setTitle("Accounts");
        ((MainActivity) getActivity()).setSupportActionBar(toolbar);
        GlobalUtils.getInstance().centerToolbarTitle(toolbar);
        img_fb_link = (ImageView) view.findViewById(R.id.img_fb_link);
        img_twitter_link = (ImageView) view.findViewById(R.id.img_twitter_link);
        img_insta_link = (ImageView) view.findViewById(R.id.img_insta_link);
        image_fb_tick = (ImageView) view.findViewById(R.id.image_fb_tick);
        img_twitter_tick = (ImageView) view.findViewById(R.id.img_twitter_tick);
        image_insta_tick = (ImageView) view.findViewById(R.id.image_insta_tick);

    }

    private void listners() {
        img_fb_link.setOnClickListener(this);
        img_twitter_link.setOnClickListener(this);
        img_insta_link.setOnClickListener(this);
        image_fb_tick.setOnClickListener(this);
        img_twitter_tick.setOnClickListener(this);
        image_insta_tick.setOnClickListener(this);
    }

    private void unLinkAccountDialog(String message, final int type) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext())
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        String fbID=new SharedPrefUtil(getContext()).getString(Constants.fB_ID);
                        String twitterID=new SharedPrefUtil(getContext()).getString(Constants.TWITTER_ID);
                        String instaID=new SharedPrefUtil(getContext()).getString(Constants.INSTA_ID);
                        if (type == 1) {

                            if (fbID!=null)
                            {
                                if (fbID!=null&&twitterID==null&&instaID==null) {
                                    new SharedPrefUtil(getContext()).deleteString(Constants.fB_ID);
                                    LoginManager.getInstance().logOut();
                                    image_fb_tick.setVisibility(View.GONE);
                                    img_fb_link.setImageResource(R.drawable.facebook_link_o);
                                    new SharedPrefUtil(getActivity()).saveBoolean(Constants.FACEBOOK_ACTIVE, false);
                                    startActivity(new Intent(getContext(),LoginActivity.class));
                                    ((MainActivity)getContext()).finish();
                                }else{
                                    new SharedPrefUtil(getContext()).deleteString(Constants.fB_ID);
                                    LoginManager.getInstance().logOut();
                                    image_fb_tick.setVisibility(View.GONE);
                                    img_fb_link.setImageResource(R.drawable.facebook_link_o);
                                }

                            }else {


                                GlobalUtils.getInstance().facebookLogin(getActivity(), mCallbackManager);
                                image_fb_tick.setVisibility(View.VISIBLE);
                                img_fb_link.setImageResource(R.drawable.facebook_link_f);


                            }

                        } else if (type == 2) {

                            if (twitterID!=null)
                            {
                                if (fbID==null&&twitterID!=null&&instaID==null) {
                                    new SharedPrefUtil(getContext()).deleteString(Constants.TWITTER_ID);
                                    img_twitter_tick.setVisibility(View.GONE);
                                    img_twitter_link.setImageResource(R.drawable.twitter_link_or);
                                    new SharedPrefUtil(getActivity()).saveBoolean(Constants.TWITTER_ACTIVE, false);
                                    startActivity(new Intent(getContext(),LoginActivity.class));
                                    ((MainActivity)getContext()).finish();

                                }else{
                                    new SharedPrefUtil(getContext()).deleteString(Constants.TWITTER_ID);
                                    img_twitter_tick.setVisibility(View.GONE);
                                    img_twitter_link.setImageResource(R.drawable.twitter_link_or);
                                    new SharedPrefUtil(getActivity()).saveBoolean(Constants.TWITTER_ACTIVE, false);
                                }


                            }else {

                              twitterAuth(getActivity(),mAuthClient);



                            }

                        } else if (type == 3) {


                            if (instaID!=null)
                            {

                                if (fbID==null&&twitterID==null&&instaID!=null) {
                                    onStart();
                                    new SharedPrefUtil(getContext()).deleteString(Constants.INSTA_ID);
                                    mApp.resetAccessToken();
                                    image_insta_tick.setVisibility(View.GONE);
                                    img_insta_link.setImageResource(R.drawable.insta_link_o);
                                    new SharedPrefUtil(getActivity()).saveBoolean(Constants.INSTA_ACTIVE, false);
                                    startActivity(new Intent(getContext(),LoginActivity.class));
                                    ((MainActivity)getContext()).finish();
                                }else{
                                    onStart();
                                    new SharedPrefUtil(getContext()).deleteString(Constants.INSTA_ID);
                                    mApp.resetAccessToken();
                                    image_insta_tick.setVisibility(View.GONE);
                                    img_insta_link.setImageResource(R.drawable.insta_link_o);
                                    new SharedPrefUtil(getActivity()).saveBoolean(Constants.INSTA_ACTIVE, false);
                                }

                            }else {

                                connectOrDisconnectUser();

                            }

                        }

                       dialog.dismiss();

                    }
                });
        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        dialog.show();

    }

    @Override
    public void onClick(View view) {
        fb_active=new SharedPrefUtil(getContext()).getBoolean(Constants.FACEBOOK_ACTIVE);
        twitter_active=new SharedPrefUtil(getContext()).getBoolean(Constants.TWITTER_ACTIVE);
        insta_active=new SharedPrefUtil(getContext()).getBoolean(Constants.INSTA_ACTIVE);
        switch (view.getId()) {

            case R.id.img_fb_link:
                if (fb_active) {

                    unLinkAccountDialog("Are you sure that you want unlink to Facebook?", 1);

                } else {

                    unLinkAccountDialog("Are you sure that you want link to Facebook?", 1);
                }
                break;
            case R.id.img_twitter_link:

                if (twitter_active) {

                    unLinkAccountDialog("Are you sure that you want unlink to Twitter?", 2);

                } else {

                    unLinkAccountDialog("Are you sure that you want link to Twitter?", 2);
                }

                break;
            case R.id.img_insta_link:
                if (insta_active) {

                    unLinkAccountDialog("Are you sure that you want unlink to Instagram?", 3);

                } else {

                    unLinkAccountDialog("Are you sure that you want link to Instagram?", 3);

                }
                break;
        }
    }




    //twitter login
    public void twitterAuth(final Activity activity, TwitterAuthClient authClient) {
        TwitterCore.getInstance().getSessionManager().getActiveSession();
        authClient.authorize(activity, new com.twitter.sdk.android.core.Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                TwitterSession user = result.data;
                long twiiter_user_id = user.getUserId();
                String twitter_user_name = user.getUserName();
                SharedPrefUtil sharedPrefUtil = new SharedPrefUtil(activity);
                sharedPrefUtil.saveString(Constants.TWITTER_ID, twiiter_user_id + "");
                sharedPrefUtil.saveBoolean(Constants.TWITTER_ACTIVE, true);
                img_twitter_tick.setVisibility(View.VISIBLE);
                img_twitter_link.setImageResource(R.drawable.twitter_link_f);

            }
            @Override
            public void failure(TwitterException exception) {
                Log.i("onFailure", exception.toString());
            }
        });
    }


}
