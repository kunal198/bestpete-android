package com.gabar.bestpete.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.gabar.bestpete.R;
import com.gabar.bestpete.activities.MainActivity;
import com.gabar.bestpete.adapters.CategoriesListAdapter;
import com.gabar.bestpete.database.AppDatabase;
import com.gabar.bestpete.database.Category;
import com.gabar.bestpete.database.SocialAccounts;
import com.gabar.bestpete.twitter.WebService;
import com.gabar.bestpete.utilities.BoldTextView;
import com.gabar.bestpete.utilities.Constants;
import com.gabar.bestpete.utilities.GlobalUtils;
import com.gabar.bestpete.utilities.SharedPrefUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

import static com.facebook.FacebookSdk.getApplicationContext;

public class CategoriesFragment extends Fragment {

    SwipeMenuListView listView;
    List<Category> list = new ArrayList<>();
    CategoriesListAdapter mAdapter;
    Toolbar toolbar;
    LinearLayout openAddCatButton;
    BoldTextView noCategory;
    AppDatabase database;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_categories, container, false);
        toolbar = rootview.findViewById(R.id.toolbar);
        toolbar.setTitle("Categories");
        ((MainActivity) getActivity()).setSupportActionBar(toolbar);
        GlobalUtils.getInstance().centerToolbarTitle(toolbar);

        database = AppDatabase.getAppDatabase(getContext());
        list = database.categoryDAO().getAllCategories();
        Collections.reverse(list);


        listView = (SwipeMenuListView) rootview.findViewById(R.id.listView);
        openAddCatButton = (LinearLayout) rootview.findViewById(R.id.openAddCatButton);
        noCategory = (BoldTextView) rootview.findViewById(R.id.noCategory);
        openAddCatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addCategoryDialog();
            }
        });

        adapterSetup();
        if (list.size() == 0) {

            noCategory.setVisibility(View.VISIBLE);
        }


        return rootview;
    }

    private void adapterSetup() {
        noCategory.setVisibility(View.GONE);
        mAdapter = new CategoriesListAdapter(getContext(), list);
        listView.setAdapter(mAdapter);
        listView.setMenuCreator(swipeCreator());
        listView.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);

        listView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {

                AlertDialog.Builder dialog = new AlertDialog.Builder(getContext())
                        .setMessage("Are you sure that you want to delete selected category?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (list.size() > 0) {

                                    Category cat = list.get(position);
                                    database.categoryDAO().delete(cat);
                                    mAdapter.removeItem(position, listView, noCategory);
                                    mAdapter = new CategoriesListAdapter(getContext(), list);
                                    listView.setAdapter(mAdapter);
                                    listView.setMenuCreator(swipeCreator());
                                    listView.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);

                                }
                                dialog.dismiss();

                            }
                        });
                dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                dialog.show();

                return false;
            }
        });

    }



    private Category insertCategory(AppDatabase database, Category category) {
        database.categoryDAO().insert(category);
        return category;
    }

    private int addUserInfo(AppDatabase database, String cat_name) {
        Category user = new Category();
        user.setCat_name(cat_name);
        insertCategory(database, user);

        Category cat = database.categoryDAO().getLastCategory();
        int id = cat.getId();
        user.setId(id);
        list.add(0, user);

        return id;

    }

    private void addCategoryDialog() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.add_cat_dialog);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialog.getWindow().setAttributes(lp);

        final EditText categoryText = (EditText) dialog.findViewById(R.id.editextCat_text);

        dialog.show();

        Button addButton = (Button) dialog.findViewById(R.id.add_button);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String category = categoryText.getText().toString();
                if (category.isEmpty()) {
                    Toast.makeText(getContext(), "Please enter category", Toast.LENGTH_SHORT).show();
                } else {

                    addUserInfo(database, category);
                    mAdapter.notifyCategories(listView, noCategory);
                    dialog.dismiss();

                }
            }
        });
        Button cancel_button = (Button) dialog.findViewById(R.id.cancel_button);
        cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    public SwipeMenuCreator swipeCreator() {
        SwipeMenuCreator swipeMenuCreator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {

                SwipeMenuItem openItem = new SwipeMenuItem(getActivity());
                openItem.setBackground(new ColorDrawable(Color.rgb(254, 2, 2)));
                openItem.setWidth(220);
                openItem.setTitle("Delete");
                openItem.setTitleSize(15);
                openItem.setTitleColor(Color.WHITE);
                menu.addMenuItem(openItem);

            }

        };
        return swipeMenuCreator;
    }


}
