package com.gabar.bestpete.fragments;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.all.All;
import com.gabar.bestpete.R;
import com.gabar.bestpete.activities.MainActivity;
import com.gabar.bestpete.adapters.FeedsAdapter;
import com.gabar.bestpete.database.AddAccounts;
import com.gabar.bestpete.database.AppDatabase;
import com.gabar.bestpete.database.Category;
import com.gabar.bestpete.utilities.Constants;
import com.gabar.bestpete.utilities.GlobalUtils;
import com.gabar.bestpete.utilities.SharedPrefUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.facebook.FacebookSdk.getApplicationContext;


public class FeedsFragment extends Fragment {
    SwipeRefreshLayout swiperefresh;
    Toolbar toolbar;
    RecyclerView feedRv;
    FeedsAdapter mAdapter;
    List<String> listOfFeeds;
    List<String> AllFeedsList=new ArrayList<>();
    AppDatabase database;
    long social_account_id;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_feeds, container, false);
        toolbar = rootview.findViewById(R.id.toolbar);
        toolbar.setTitle("Latest Feeds");
        ((MainActivity) getActivity()).setSupportActionBar(toolbar);
        GlobalUtils.getInstance().centerToolbarTitle(toolbar);
        init(rootview);
        database = AppDatabase.getAppDatabase(getContext());

        Bundle bundle = getArguments();

        int id = bundle.getInt("Category_id");

        List<AddAccounts> listOfSocialAccountsIds = database.socialDao().getAllAccounts(id);

        if (listOfSocialAccountsIds != null) {
            for (int i = 0; i < listOfSocialAccountsIds.size(); i++) {
                social_account_id = listOfSocialAccountsIds.get(i).getAccountId();
                Log.d("ACcount_ID : ", "==" + social_account_id);
                getpageFeeds(social_account_id);
            }




        }

        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                getpageFeeds(social_account_id);
            }
        });


        return rootview;
    }

    public void init(View view) {
        feedRv = (RecyclerView) view.findViewById(R.id.feedRv);
        swiperefresh = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);
    }

    private void adapterSetup() {
        mAdapter = new FeedsAdapter(getContext(),AllFeedsList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        feedRv.setLayoutManager(mLayoutManager);
        feedRv.setItemAnimator(new DefaultItemAnimator());
        feedRv.setAdapter(mAdapter);
    }


    public void getpageFeeds(long id) {
        swiperefresh.setRefreshing(true);
        listOfFeeds = new ArrayList<>();
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                id+ "/feed",
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        if (response != null) {
                            swiperefresh.setRefreshing(false);
                            Log.d("FEED_RESPONSE : ", "==" + response);
                            JSONObject mainObj = response.getJSONObject();
                            JSONArray mainArray = mainObj.optJSONArray("data");
                            listOfFeeds=new ArrayList<>();
                            for (int i = 0; i < mainArray.length(); i++) {
                                JSONObject jsonObject = mainArray.optJSONObject(i);
                                String message = jsonObject.optString("message");

                                Log.d("Message ", "==" + message);

                                listOfFeeds.add(message);

                            }

                            AllFeedsList.addAll(listOfFeeds);

                            Log.d("FeedsList ", "==" + AllFeedsList.size());
                            adapterSetup();
                        }
                    }
                }
        ).executeAsync();
    }


    private void imageDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        final AlertDialog dialog = builder.create();
        LayoutInflater inflater = getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.inflater_image_dialog, null);
        dialog.setView(dialogLayout);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.show();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface d) {
                ImageView image = (ImageView) dialog.findViewById(R.id.goProDialogImage);
                Bitmap icon = BitmapFactory.decodeResource(getContext().getResources(),
                        R.drawable.sports);
                float imageWidthInPX = (float) image.getWidth();

                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(Math.round(imageWidthInPX),
                        Math.round(imageWidthInPX * (float) icon.getHeight() / (float) icon.getWidth()));
                image.setLayoutParams(layoutParams);


            }
        });
    }

}
