package com.gabar.bestpete.utilities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.media.Image;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.gabar.bestpete.R;
import com.gabar.bestpete.activities.LoginActivity;
import com.gabar.bestpete.activities.MainActivity;
import com.gabar.bestpete.activities.SplashActivity;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;


/**
 * Author: Sanjeev Sharma
 * Created on: 13/12/2017
 * Project: Bestpete App
 */

public class GlobalUtils {
    private static GlobalUtils instance = new GlobalUtils();
    static ProgressDialog progressDialog;


    //get instance of this class
    public static GlobalUtils getInstance() {
        return instance;
    }

    //show progress dialog
    public void showProgressDialog(Context context, String message) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.show();

    }

    public void hideProgressDialog() {
        progressDialog.cancel();
    }

    //show alert message dialog
    public void showMessageDialog(Context context, String message, String title) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(context);
        }
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    //check internet conncection
    public boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    //no iternet dialog
    public void ShowNoIternetDialog(final Activity mContext, String message) {
        try {
            Log.i("message===", message);
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.CustomPopUpThemeBlue);
            //  builder.setTitle("No Internet Connection");
            builder.setMessage(message);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (mContext instanceof SplashActivity){
                        System.exit(0);
                    }else {
                        dialog.dismiss();
                    }

                }
            });
            builder.setCancelable(false);
            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //replace fragment
    public void replaceFragment(AppCompatActivity activity, Fragment fragment) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =
                fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_container, fragment).addToBackStack(null);
        fragmentTransaction.commit();
    }

    //set toolbar title in center
    public void centerToolbarTitle(@NonNull final Toolbar toolbar) {
        final CharSequence title = toolbar.getTitle();
        final ArrayList<View> outViews = new ArrayList<>(1);
        toolbar.findViewsWithText(outViews, title, View.FIND_VIEWS_WITH_TEXT);
        if (!outViews.isEmpty()) {
            final TextView titleView = (TextView) outViews.get(0);
            titleView.setGravity(Gravity.CENTER);
            final Toolbar.LayoutParams layoutParams = (Toolbar.LayoutParams) titleView.getLayoutParams();
            layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
            toolbar.requestLayout();

        }
    }

    //print facebook key hash
    public void printKeyHash(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    "com.gabar.bestpete", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:",
                        Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

    //facebook login
    public void facebookLogin(final Activity activity, CallbackManager callbackManager) {
        if (AccessToken.getCurrentAccessToken() == null || AccessToken.getCurrentAccessToken().isExpired()) {
            LoginManager.getInstance().logInWithReadPermissions(activity, Arrays.asList("public_profile", "user_friends", "email"));
            LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    Log.d("TAG", "RESULT:" + loginResult);
                    loginResult.getAccessToken();
                    Log.d("TOKEN", "" + loginResult.getAccessToken());
                    GraphRequest request = GraphRequest.newMeRequest(
                            loginResult.getAccessToken(),
                            new GraphRequest.GraphJSONObjectCallback() {
                                @Override
                                public void onCompleted(JSONObject user, GraphResponse response) {
                                    try {

                                        Log.d("TAG", "LoginResponse:" + response);

                                        String userid = user.getString("id");
                                        String first_name = user.getString("first_name");
                                        String last_name = user.getString("last_name");

                                        SharedPrefUtil sharedPrefUtil = new SharedPrefUtil(activity);
                                        sharedPrefUtil.saveString(Constants.fB_ID, userid);
                                        sharedPrefUtil.saveBoolean(Constants.FACEBOOK_ACTIVE, true);

                                        if (activity instanceof LoginActivity) {
                                            activity.startActivity(new Intent(activity, MainActivity.class));
                                            activity.overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                                            activity.finish();
                                        }


                                    } catch (Exception x) {
                                        x.getCause();
                                    }

                                }
                            });
                    Bundle parameters = new Bundle();
                    parameters.putString("fields", "id,first_name,last_name,email,gender,birthday,picture");
                    request.setParameters(parameters);
                    request.executeAsync();
                }

                @Override
                public void onCancel() {

                }

                @Override
                public void onError(FacebookException exception) {
                    if (exception instanceof FacebookAuthorizationException) {
                        if (AccessToken.getCurrentAccessToken() != null) {
                            LoginManager.getInstance().logOut();
                        }
                    }
                }
            });
        } else {
            GraphRequest request = GraphRequest.newMeRequest(
                    AccessToken.getCurrentAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject user,
                                GraphResponse response) {

                            try {
                                Log.d("TAG", "LoginResponse:" + response);
                                String userid = user.getString("id");
                                String first_name = user.getString("first_name");
                                String last_name = user.getString("last_name");
                                SharedPrefUtil sharedPrefUtil = new SharedPrefUtil(activity);
                                sharedPrefUtil.saveString(Constants.fB_ID, userid);
                                sharedPrefUtil.saveBoolean(Constants.FACEBOOK_ACTIVE, true);

                                if (activity instanceof LoginActivity) {
                                    activity.startActivity(new Intent(activity, MainActivity.class));
                                    activity.overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                                    activity.finish();
                                }


                            } catch (Exception e) {

                            }
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,first_name,last_name,name,email,gender,birthday,picture");
            request.setParameters(parameters);
            request.executeAsync();
        }
    }

    //twitter login
    public void twitterAuth(final Activity activity, TwitterAuthClient authClient) {
        TwitterCore.getInstance().getSessionManager().getActiveSession();
        authClient.authorize(activity, new com.twitter.sdk.android.core.Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                TwitterSession user = result.data;
                long twiiter_user_id = user.getUserId();
                String twitter_user_name = user.getUserName();
                SharedPrefUtil sharedPrefUtil = new SharedPrefUtil(activity);
                sharedPrefUtil.saveString(Constants.TWITTER_ID, twiiter_user_id + "");
                sharedPrefUtil.saveBoolean(Constants.TWITTER_ACTIVE, true);

                if (activity instanceof LoginActivity){
                    activity.startActivity(new Intent(activity, MainActivity.class));
                    activity.overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                    activity.finish();
                }

            }

            @Override
            public void failure(TwitterException exception) {
                Log.i("onFailure", exception.toString());
                Toast.makeText(activity, "Login failed...", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
